<?php
$debug=false;
$json = array();
$times = array();
$site = file_get_contents("http://poezdato.net/raspisanie-poezdov/vishnevoe--kiev-pass/");

$regex = '/departure"[^"]+\"timetable_time\">(\d\d\.\d\d)</'; 

preg_match_all($regex, $site, $matches);

$now = new DateTime(date('H:i',(string)intval(time())+7200));

foreach($matches[1] as $time){
    $time = str_replace('.', ':', $time);
    
    
        
    $tempTime = new DateTime($time);
    $result =  $now->diff($tempTime);

    $suffix = ( $result->invert ? ' ago' : '' );

    if($debug){
        echo $now->format('H:i').'<br>'.
        $tempTime->format('H:i').'<br>'.
        $result->format('%h:%i')." $suffix".'<br><hr>';
    }
    
    if(!$result->invert && ($result->i>20 || $result->h==1)){
        $json[]= $result->format('%h:%i');
        
    }
}

 echo json_encode($json);
?>