# LilHelper #

This is a small project, that I will use in my daily life to help me check the weather, train departure list and traffic jams. Some more features will be added, if I'll be smart enough :)

But mostly, this project is made for sharpening git skills :D

### Who do I talk to? ###

* If you have any ideas, feel free to e-mail me.

### Links ###

* The site is up and running, check [here](http://teracon.cf/)

